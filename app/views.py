import flask
import covid


views = flask.Blueprint("views", __name__)


@views.route("/", methods=["GET", "POST"])
def get_info_covid19():
    if flask.request.method == "POST":
        country = flask.request.form.get("country", "ireland")
    else:
        country = flask.request.form.get("country", "ireland")
    api = covid.api.CovId19Data(force=False)

    try:
        res = api.filter_by_country(country)
    except covid.lib.errors.CountryNotFound:
        flask.abort(404, "Country not found")

    return flask.render_template("index.html", country=country, info_country=res)
